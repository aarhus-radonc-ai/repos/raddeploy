import logging
import os
import signal
import threading
import time


from RadDeployLib.conf import load_configs
from RadDeployLib.log import init_logger
from RadDeployLib.log.mq_handler import MQHandler
from RadDeployLib.mq import MQSub
from RadDeployLib.mq import PubModel, SubModel

from db import Database
from api import API
from scheduling import Scheduler


class Main:
    def __init__(self, config):
        signal.signal(signal.SIGTERM, self.stop)

        self.running = None
        self.mq_handler = MQHandler(
            rabbit_hostname=config["RABBIT_HOSTNAME"],
            rabbit_port=int(config["RABBIT_PORT"]),
            pub_models=[PubModel(**d) for d in config["LOG_PUB_MODELS"]]
        )
        init_logger(name=None,  # init root logger,
                    log_format=config["LOG_FORMAT"],
                    log_dir=config["LOG_DIR"],
                    mq_handler=self.mq_handler)

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(int(config["LOG_LEVEL"]))

        self.db = Database(database_url=config["DATABASE_URL"],
                           log_level=int(config["LOG_LEVEL"]))

        self.scheduler = Scheduler(log_level=int(config["LOG_LEVEL"]),
                                   database=self.db)

        self.mq_fingerprinter = MQSub(rabbit_hostname=config["RABBIT_HOSTNAME"],
                        rabbit_port=int(config["RABBIT_PORT"]),
                        sub_models=[SubModel(**d) for d in config["FINGERPRINTER_SUB_MODELS"]],
                        pub_models=[PubModel(**d) for d in config["PUB_MODELS"]],
                        work_function=self.scheduler.fingerprinter_entrypoint,
                        sub_prefetch_value=int(config["SUB_PREFETCH_COUNT"]),
                        sub_queue_kwargs=config["SUB_QUEUE_KWARGS"],
                        log_level=int(config["LOG_LEVEL"]))

        self.mq_consumer = MQSub(rabbit_hostname=config["RABBIT_HOSTNAME"],
                        rabbit_port=int(config["RABBIT_PORT"]),
                        sub_models=[SubModel(**d) for d in config["CONSUMER_SUB_MODELS"]],
                        pub_models=[PubModel(**d) for d in config["PUB_MODELS"]],
                        work_function=self.scheduler.consumer_entrypoint,
                        sub_prefetch_value=int(config["SUB_PREFETCH_COUNT"]),
                        sub_queue_kwargs=config["SUB_QUEUE_KWARGS"],
                        log_level=int(config["LOG_LEVEL"]))

        self.api_db = Database(database_url=config["DATABASE_URL"],
                                     log_level=int(config["LOG_LEVEL"]))
        self.api = API(db=self.api_db,
                       log_level=int(config["LOG_LEVEL"]))

    def start(self, blocking=True):
        self.running = True
        self.mq_fingerprinter.start()
        self.mq_consumer.start()
        self.mq_handler.start()
        self.api.start()

        while self.running and blocking:
            time.sleep(1)

    def stop(self, signalnum=None, stack_frame=None):
        self.running = False
        self.api.terminate()
        self.mq_fingerprinter.stop()
        self.mq_consumer.stop()
        self.mq_handler.stop()

        self.mq_fingerprinter.join()
        self.mq_consumer.join()
        self.mq_handler.join()


if __name__ == "__main__":
    config = load_configs(os.environ["CONF_DIR"], os.environ["CURRENT_CONF"])

    m = Main(config=config)
    m.start()
