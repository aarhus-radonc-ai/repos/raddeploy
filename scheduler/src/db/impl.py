import datetime
import json
import logging

import sqlalchemy
from RadDeployLib.data_structures.service_contexts import FlowContext

from RadDeployLib.data_structures.service_contexts import JobContext
from RadDeployLib.db import DatabaseBoilerplate

from RadDeployLib.data_structures.service_contexts import FinishedFlowContext
from RadDeployLib.mq import PublishContext
from .db_models import Base, DBFlow, DBJob, FlowMountMapping


class Database(DatabaseBoilerplate):
    def __init__(self, database_url: str, declarative_base=Base, log_level=10):
        super().__init__(database_url=database_url,
                         declarative_base=declarative_base,
                         db_model_module="db.db_models",
                         log_level=log_level)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)

    def add_db_flow(self, flow_context: FlowContext) -> DBFlow:
        # Add the flow
        db_flow = self.add(DBFlow(flow_context_json=flow_context.model_dump_json()))

        # Add all models
        for model in flow_context.flow.models:
            self.add(
                DBJob(
                    db_flow_id=db_flow.id,
                    model_json=model.model_dump_json(),
                )
            )

        # Add the 'src' tar file
        self.add_mount_mapping(db_flow_id=db_flow.id,
                                  name="src",
                                  uid=flow_context.src_uid)

        return self.get_by_kwargs("DBFlow", {"id": db_flow.id}, first=True)


    def add_mount_mapping(self, db_flow_id: int, name: str, uid: str) -> FlowMountMapping:
        db_mount_mapping = FlowMountMapping(
            name=name,
            uid=uid,
            db_flow_id=db_flow_id
        )
        try:
            return self.add(db_mount_mapping)
        except sqlalchemy.exc.IntegrityError:
            self.logger.debug("Mapping exists - passing")
        except Exception as e:
            raise e

    def update_db_mount_mapping_from_job_context_output(self, job_context: JobContext):
        db_job = self.get_by_kwargs("DBJob", {"id": job_context.id}, first=True)

        for name, uid in job_context.mount_mapping.items():
            self.logger.debug(f"Adding mount mapping to flow {job_context.id}, name: {name}, uid: {uid}")

            self.add_mount_mapping(db_flow_id=db_job.db_flow_id,
                                   name=name,
                                   uid=uid)

    def yield_runnable_jobs(self):
        # Find jobs that are runnable and has not been dispatched before
        job: DBJob
        for job in self.get_by_kwargs("DBJob", where_kwargs={"status_code": -3}):
            if job.has_input_mounts_available:
                yield job


    def yield_finished_flows(self):
        for db_flow in self.get_by_kwargs("DBFlow", where_kwargs={"is_published": False}):
            if db_flow.status in ["SUCCESS", "FAIL"]: # Trying to send "FAIL" despite fail. Might get partial output from "dst*"
                yield db_flow

    def submit_flow(self, db_flow: DBFlow):
        db_flow = self.update_by_kwargs("DBFlow",
                                 where_kwargs={"id": db_flow.id},
                                 update_kwargs={"is_published": True},
                                 first=True)

        finished_flow_context = FinishedFlowContext(**db_flow.flow_context.model_dump(),
                                                   mount_mapping=db_flow.mount_mapping)

        return PublishContext(body=finished_flow_context.model_dump_json().encode(),
                                 pub_model_routing_key=db_flow.status,
                                 priority=finished_flow_context.flow.priority)

    def submit_job(self, job: DBJob):
            job = self.update_by_kwargs("DBJob",
                                     where_kwargs={"id": job.id},
                                     update_kwargs={
                                        "status_code": -2 # Submitted
                                     },
                                     first=True)

            job_context = JobContext(
                id=job.id,
                model=job.model,
                mount_mapping=job.db_flow.mount_mapping,
                flow=json.loads(job.db_flow.flow_context_json)["flow"],
                status_code=job.status_code,
            )
            return PublishContext(
                body=job_context.model_dump_json().encode(),
                pub_model_routing_key="GPU" if job.model.gpu else "CPU",
                priority=job.db_flow.priority
            )

