import datetime
import json
from typing import List, Dict

import numpy as np
import sqlalchemy
from sqlalchemy import ForeignKey, UniqueConstraint, select
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column, relationship

from RadDeployLib.data_structures.service_contexts import FlowContext
from RadDeployLib.db import Base

from RadDeployLib.data_structures.flow import Model

status_mapping = {
    -3: "Pending",
    -2: "Submitted",
    -1: "Running",
    0: "Succeded",
    1: "Failed",
    137: "Killed",
    255: "Container Error",
    400: "Upstream Error",
    401: "Container not found or accessible",
    500: "Timeout",
    600: "Revoked"
}

class DBFlow(Base):
    __tablename__ = "db_flows"
    flow_context_json: Mapped[str] = mapped_column(sqlalchemy.types.Text)
    db_jobs: Mapped[List["DBJob"]] = relationship("DBJob",
                                                back_populates="db_flow",
                                                lazy="joined",
                                                join_depth=2)
    _mount_mapping: Mapped[List["FlowMountMapping"]] = relationship("FlowMountMapping",
                                                                 lazy="joined",
                                                                 join_depth=2)
    is_published: Mapped[bool] = mapped_column(default=False)


    def as_dict(self) -> Dict:
        fc = self.flow_context
        d = {}
        d["id"] = self.id
        d["ts"] = self.ts
        d["flow"] = fc.flow.model_dump()
        d["is_finished"] = self.is_finished
        d["is_failed"] = self.is_failed
        d["is_published"] = self.is_published
        d["priority"] = self.priority
        d["db_jobs"] = [job.as_dict() for job in self.db_jobs]
        return d

    @property
    def mount_mapping(self):
        return {mm.name: mm.uid for mm in self._mount_mapping}

    @property
    def flow_context(self):
        return FlowContext(**json.loads(self.flow_context_json))

    @property
    def is_finished(self):
        for db_job in self.db_jobs:
            if not db_job.is_finished:
                return False
        else:
            return True

    @property
    def is_failed(self):
        statuses = {db_job.status_code for db_job in self.db_jobs}
        if statuses.issubset({-3, -2, -1, 0}):
            return False
        else:
            return True

    @property
    def priority(self):
        return self.flow_context.flow.priority


class FlowMountMapping(Base):
    __tablename__ = "db_flow_mount_mappings"
    db_flow_id: Mapped[int] = mapped_column(ForeignKey('db_flows.id'))
    db_flow = relationship("DBFlow", uselist=False, back_populates="_mount_mapping")
    name: Mapped[str] = mapped_column(sqlalchemy.types.String(length=128))
    uid: Mapped[str] = mapped_column(sqlalchemy.types.String(length=128))
    __table_args__ = (UniqueConstraint('db_flow_id', 'name', name='_mount_mapping_uc'),)

class DBJobStatus(Base):
    """
    docker container codes:
        -3: pending - not queued,
        -2: queued,
        -1: running,
        0: finished successfully
        400: Unable to execute
        500: Timeout
        600: Revoked
    """
    __tablename__ = "db_job_statuses"
    db_job_id: Mapped[int] = mapped_column(ForeignKey('db_jobs.id'))
    status_code: Mapped[int] = mapped_column(default=-3)


class DBJob(Base):
    __tablename__ = "db_jobs"
    # Static variables
    db_flow_id: Mapped[int] = mapped_column(ForeignKey("db_flows.id"))
    db_flow: Mapped["DBFlow"] = relationship(back_populates="db_jobs",
                                             uselist=False,
                                             lazy="joined",
                                             join_depth=2)
    model_json: Mapped[str] = mapped_column(sqlalchemy.types.Text)

    # Dynamic variables
    _status_codes: Mapped[List["DBJobStatus"]] = relationship(uselist=True, lazy="joined", join_depth=2, order_by="DBJobStatus.status_code")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._status_codes.append(DBJobStatus()) # Add default status
    @property
    def time_sorted_status_codes(self):
        return sorted(self._status_codes, key=lambda x: x.ts)

    @property
    def heartbeat(self):
        return self.time_sorted_status_codes[-1].ts

    def get_timestamp_of_status_code(self, status_code, comparison_func=np.equal):
        """ Sorts by timestamp """
        for status in self.time_sorted_status_codes:
            if comparison_func(status.status_code, status_code):
                return status.ts
        else:
            return None

    def as_dict(self) -> Dict:
        d = {}
        d["id"] = self.id
        d["ts"] = self.ts
        d["db_flow_id"] = self.db_flow_id
        d["status_code"] = self.status_code
        d["heartbeat"] = self.heartbeat
        d["is_finished"] = self.is_finished
        d["model"] = json.loads(self.model_json)

        return d

    @hybrid_property
    def status_code(self):
        """ Return highest status code """
        return self._status_codes[-1].status_code

    @status_code.setter
    def status_code(self, value: int):
        self._status_codes.append(DBJobStatus(db_job_id=self.id, status_code=value))

    @status_code.expression
    def status_code(self):
        return (
            select(DBJobStatus.status_code)
            .where(DBJobStatus.db_job_id == self.id)
            .order_by(DBJobStatus.status_code.desc())
            .limit(1)
            .scalar_subquery()
        )

    @property
    def model(self):
        return Model(**json.loads(self.model_json))

    @property
    def timeout(self):
        return self.model.timeout

    @property
    def input_mount_keys(self):
        return set(self.model.input_mount_keys)

    @property
    def output_mount_keys(self):
        return set(self.model.output_mount_keys)

    @property
    def has_input_mounts_available(self) -> bool:
        return set(self.input_mount_keys).issubset(set(self.db_flow.mount_mapping.keys()))

    @property
    def has_output_mounts_available(self) -> bool:
        return set(self.output_mount_keys).issubset(set(self.db_flow.mount_mapping.keys()))

    @property
    def is_finished(self) -> bool:
        return self.status_code >= 0

    @property
    def is_runnable(self) -> bool:
        return (self.has_input_mounts_available and not self.is_finished)

    @property
    def is_timeout(self):
        return (datetime.datetime.now() - self.ts).total_seconds() > self.timeout
