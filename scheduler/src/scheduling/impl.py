import json
import logging
from datetime import datetime
from typing import Iterable

from RadDeployLib.data_structures.service_contexts import FlowContext
from RadDeployLib.data_structures.service_contexts.models import JobContext, FinishedFlowContext
from RadDeployLib.mq.mq_models import PublishContext
from db import Database, DBFlow, DBJob


class Scheduler:
    def __init__(self,
                 database: Database,
                 log_level: int = 20):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)
        self.db = database
        self.main_loop_last_run = None

    def fingerprinter_entrypoint(self, basic_deliver, body) -> Iterable[PublishContext]:
        """
        This method inserts flow_contexts that are coming from the fingerprinters
        """
        self.logger.debug(f"Scheduler received message on {basic_deliver}")
        flow_context = FlowContext(**json.loads(body.decode()))

        # Add Flow to DB when orchestration sees it for the first time. This also adds DBJobs
        self.logger.debug(f"Initiating received message in DB")
        self.db.add_db_flow(flow_context)

        # Any updates
        yield from self.main_loop()

    def consumer_entrypoint(self, basic_deliver, body) -> Iterable[FinishedFlowContext]:
        """
        This method provides the way to update the DB with stuff from the consumers
        """
        self.logger.debug(f"Scheduler received message on {basic_deliver}")
        job_context = JobContext(**json.loads(body.decode()))

        # Update scheduler DB with incoming job (For any status)
        self.logger.debug(f"Updating DB according to received job_context")
        self.db.update_by_kwargs(DBJob,
                                 {"id": job_context.id},
                                 {"status_code": job_context.status_code})

        # Updating DB with output mappings from this model
        self.logger.debug(f"Updating DB with output mappings from this model")

        # Get the relevant flow_id and insert in DB
        db_job = self.db.get_by_kwargs("DBJob", {"id": job_context.id}, first=True)
        for src, uid in job_context.mount_mapping.items():
            self.logger.debug(f"Adding mount mapping to flow {job_context.id}, src: {src}, uid: {uid}")
            self.db.add_mount_mapping(
                db_flow_id=db_job.db_flow_id,
                name=src,
                uid=uid
            )

        yield from self.main_loop()

    def prepare_flow_for_pub(self, db_flow: DBFlow):
        db_flow = self.db.update_by_kwargs("DBFlow",
                                 where_kwargs={"id": db_flow.id},
                                 update_kwargs={"is_published": True},
                                 first=True)

        finished_flow_context = FinishedFlowContext(**db_flow.flow_context.model_dump(),
                                                   mount_mapping=db_flow.mount_mapping)

        return PublishContext(body=finished_flow_context.model_dump_json().encode(),
                                 pub_model_routing_key="FAIL" if db_flow.is_failed else "SUCCESS",
                                 priority=finished_flow_context.flow.priority)

    def prepare_job_for_pub(self, db_job: DBJob):
            db_job = self.db.update_by_kwargs("DBJob",
                                     where_kwargs={"id": db_job.id},
                                     update_kwargs={
                                        "status_code": -2 # Submitted
                                     },
                                     first=True)
            return PublishContext(
                pub_model_routing_key="GPU" if db_job.model.gpu else "CPU",
                priority=json.loads(db_job.db_flow.flow_context_json)["flow"]["priority"],
                body=JobContext(
                            id=db_job.id,
                            model=db_job.model,
                            mount_mapping=db_job.db_flow.mount_mapping,
                            flow=json.loads(db_job.db_flow.flow_context_json)["flow"],
                            status_code=db_job.status_code
                ).model_dump_json().encode()
            )

    def handle_timeout_jobs(self):
        # If one job fails, all subsequent jobs should fail too
        for flow in self.db.get_by_kwargs("DBFlow", {"is_published": False}):
            # If Fail, invalidate all subsequent
            if not flow.is_finished:
                for job in flow.db_jobs:
                    if not job.is_finished and job.is_timeout:
                        self.db.update_by_kwargs("DBJob",
                                                 {"id": job.id},
                                                 {"status_code": 500})

    def handle_failed_jobs(self):
        # If one job fails, all subsequent jobs should fail too
        for flow in self.db.get_by_kwargs("DBFlow", {"is_published": False}):
            # If Fail, invalidate all subsequent
            if flow.is_failed:
                for job in flow.db_jobs:
                    if job.status_code < 0:
                        self.db.update_by_kwargs("DBJob",
                                                 {"id": job.id},
                                                 {"status_code": 400})

    def yield_finished_flows(self):
        for flow in self.db.get_by_kwargs("DBFlow", where_kwargs={"is_published": False}):
            if flow.is_finished:
                yield flow

    def yield_runnable_jobs(self):
        # Find jobs that are runnable and has not been dispatched before
        for job in self.db.get_by_kwargs(DBJob, where_kwargs={"status_code": -3}):
            if job.is_runnable:
                yield job


    def main_loop(self):
        # To do: Resubmit db_jobs which are timed_out
        self.logger.info("Handling failed jobs")
        self.handle_failed_jobs()

        self.logger.info("Handling timeout jobs")
        self.handle_timeout_jobs()

        self.logger.info("Yield eligible jobs")
        for job in self.yield_runnable_jobs():
            yield self.prepare_job_for_pub(job)

        self.logger.info("Yield finished flows")
        for flow in self.yield_finished_flows():
            yield self.prepare_flow_for_pub(flow)

        self.main_loop_last_run = datetime.now()



