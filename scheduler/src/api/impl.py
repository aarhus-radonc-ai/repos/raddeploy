import logging
import multiprocessing
from typing import Optional

import numpy as np
import uvicorn
from db import Database, status_mapping
from fastapi import FastAPI, HTTPException

from db import DBFlow, DBJob


def generate_patient_pseudonym(context):
    if "patient_id" in context and "patient_name" in context:
        pid = context["patient_id"]
        name = context["patient_name"]
        try:
            name = [n[0] for n in name.replace("^", " ").split(" ")]
            return pid[:4] + "".join(name)
        except:
            return "invalid pseudonym"
    else:
        return "anon"

def format_db_flow(db_flow: DBFlow):
    flow_context = db_flow.flow_context
    return {
        "id": db_flow.id,
        "ts": db_flow.ts.strftime("%Y-%m-%d %H:%M:%S"),
        "name": flow_context.flow.name,
        "version": flow_context.flow.version,
        "priority": flow_context.flow.priority,
        "patientID": generate_patient_pseudonym(flow_context.context),
        "destinations": [dest.model_dump() for dest in flow_context.flow.destinations],
        "finished": "Yes" if db_flow.is_finished else "No",
        "published": "Yes" if db_flow.is_published else "No",
    }
def format_db_job(db_job: DBJob):
    started_ts = db_job.get_timestamp_of_status_code(-1)
    finished_ts = db_job.get_timestamp_of_status_code(0, np.greater_equal)
    if started_ts and finished_ts:
        runtime = (finished_ts - started_ts).total_seconds()
        runtime = 0 if runtime < 0 else runtime
    else:
        runtime = "N/A"

    return {
        "id": db_job.id,
        "flowID": db_job.db_flow_id,
        "startTime": "N/A" if not started_ts else started_ts.strftime("%Y-%m-%d %H:%M:%S"),
        "finishTime": "N/A" if not finished_ts else finished_ts.strftime("%Y-%m-%d %H:%M:%S"),
        "runtime": runtime,
        "image": db_job.model.docker_kwargs["image"],
        "priority": db_job.db_flow.priority,
        "status": db_job.status_code if not db_job.status_code in status_mapping.keys() else status_mapping[db_job.status_code],
        "heartbeat": db_job.heartbeat.strftime("%Y-%m-%d %H:%M:%S"),
    }

class API(multiprocessing.Process):
    def __init__(self,
                 db: Database,
                 log_level: int = 20, **extra):
        super().__init__(**extra)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)

        self.db = db
        self.should_exit = False
        self.app = FastAPI()

        @self.app.get("/")
        def hello_world():
            return {"msg": "hello scheduler API"}

        @self.app.get("/db_flows")
        def get_flows():
            return [f.as_dict() for f in self.db.get_by_kwargs("DBFlow")]

        @self.app.get("/db_flow/{id}")
        def get_flow(id: int):
            flow = self.db.get_by_kwargs("DBFlow",
                                         where_kwargs={"id": id}, first=True)
            if flow:
                return flow.as_dict()
            else:
                raise HTTPException(status_code=404, detail="Flow not found")

        @self.app.get("/db_jobs")
        def get_db_jobs():
            return [j.as_dict() for j in self.db.get_by_kwargs("DBJob")]

        @self.app.get("/db_job/{id}")
        def get_db_job(id: int):
            job = self.db.get_by_kwargs("DBJob",
                                         where_kwargs={"id": id}, first=True)
            if job:
                return job.as_dict()
            else:
                raise HTTPException(status_code=404, detail="Flow not found")

        @self.app.post("/db_flow/{id}/revoke")
        def revoke_flow(id: int):
            flow = self.db.get_by_kwargs("DBFlow", {"id": id})
            if not flow:
                raise HTTPException(status_code=404, detail="Flow not found")

            jobs = self.db.get_by_kwargs("DBJob",
                                         where_kwargs={"db_flow_id": id})
            for job in jobs:
                if not job.is_finished:
                    self.db.update_by_kwargs("DBJob",
                                             where_kwargs={"db_flow_id": id},
                                             update_kwargs={"status_code": 600}) # Revoked
            return get_flow(id)

        @self.app.get("/dashboard/db_jobs")
        def dashboard_db_jobs(n_recent: Optional[int] = None):
            return [format_db_job(job) for job in self.db.get_by_kwargs("DBJob", n_recent=n_recent)]

        @self.app.get("/dashboard/db_job/{id}")
        def dashboard_db_job(id: int):
            return format_db_job(self.db.get_by_kwargs("DBJob", {"id": id}, first=True))

        @self.app.get("/dashboard/db_flows")
        def dashboard_db_flows(n_recent: Optional[int] = None):
            return [format_db_flow(flow) for flow in self.db.get_by_kwargs("DBFlow", n_recent=n_recent)]

        @self.app.get("/dashboard/db_flow/{id}")
        def dashboard_db_flow(id: int):
            return format_db_flow(self.db.get_by_kwargs("DBFlow", {"id": id}, first=True))

    def run(self):
        uvicorn.run(self.app, host="0.0.0.0", port=80)