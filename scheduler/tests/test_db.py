import pytest
import sqlalchemy

from RadDeployLib.data_structures.flow import Flow
from RadDeployLib.test_utils.fixtures import *
from .fixtures import db, db_flow

def test_add_db_flow(db, db_flow):
    assert db_flow.id
    assert Flow.is_valid_dag(db_flow.flow_context.flow.models)
    assert len(db_flow.db_jobs) != 0
    assert len(db_flow.db_jobs[0].input_mount_keys) != 0

def test_add_mount_mapping(db, dag_flow_context):
    # Add Flow
    flow = db.add_db_flow(dag_flow_context)

    # No mapping added
    assert len(flow.mount_mapping) == 1

    # Add mapping and assert now 1 mapping is there
    db.add_mount_mapping(flow.id, "dst", "VeryUID-UID")
    echo_flow = db.get_by_kwargs("DBFlow", {"id": flow.id}, first=True)
    assert len(echo_flow.mount_mapping) == 2

def test_add_mount_mapping_uc(db, dag_flow_context):
    flow = db.add_db_flow(dag_flow_context)
    assert len(flow.mount_mapping) == 1

    # First add
    db.add_mount_mapping(flow.id, name="src", uid="VeryUID-UID")

    # Assert still only one src
    flow = db.get_by_kwargs("DBFlow", {"id": flow.id}, first=True)
    assert len(flow.mount_mapping) == 1


def test_status_code_default(db, db_flow):
    # assert default status code
    assert db_flow.db_jobs[0].status_code == -3

    # Assert that it is inserted as a DBJobStatus in _status_codes
    assert len(db_flow.db_jobs[0]._status_codes) == 1

def test_status_code_setter(db, db_flow):
    # Assert status_code setter works
    db.update_by_kwargs("DBJob", {}, {"status_code": 100})
    jobs = db.get_by_kwargs("DBJob")
    for job in jobs:
        assert job.status_code == 100
        assert len(job._status_codes) == 2

def test_status_code_query(db, db_flow):
    db.update_by_kwargs("DBJob", {}, {"status_code": 100})
    jobs = db.get_by_kwargs("DBJob", {"status_code": 100})
    for job in jobs:
        assert job.status_code == 100
        assert len(job._status_codes) == 2

def test_flow_is_finished(db_flow, db):
    # Assert not finished
    flow = db.get_by_kwargs("DBFlow", {"id": db_flow.id}, first=True)
    assert not flow.is_finished

    # Update all jobs under the flow to finished and check that flow is marked as finished
    db.update_by_kwargs("DBJob", {"db_flow_id": flow.id}, {"status_code": 0})
    flow = db.get_by_kwargs("DBFlow", {"id": flow.id}, first=True)
    assert flow.is_finished

def test_has_input_mounts(db_flow, db):
    assert "src" in db_flow.mount_mapping.keys()
    assert db_flow.db_jobs[0].has_input_mounts_available


def test_runnable(db_flow, db):
    # Get and check before insert
    # has_input_mounts_available mount mapping
    echo_db_flow = db.get_by_kwargs("DBFlow", {"id": db_flow.id}, first=True)
    db_job = echo_db_flow.db_jobs[0]
    assert db_job.is_runnable


    db_job = db.update_by_kwargs("DBJob",
                                       {"id": db_job.id},
                                       {"status_code": 0},
                                       first=True)
    assert not db_job.is_runnable
