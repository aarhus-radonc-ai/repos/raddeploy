import pytest

from .fixtures import db, db_flow
from api import API
from fastapi.testclient import TestClient

from RadDeployLib.test_utils.fixtures import  *

@pytest.fixture
def api(db):
    api = API(db)
    return api

@pytest.fixture
def test_api(api):
    return TestClient(api.app)

def test_hello_world(test_api):
    res = test_api.get("/")
    assert "hello scheduler API" == res.json()["msg"]

def test_revoke_flow(test_api, api, db, db_flow):
    res = test_api.post(f"/db_flow/{db_flow.id}/revoke")
    res.raise_for_status()

    res = test_api.get(f"/db_flow/{db_flow.id}")
    print(res.json())
    assert res.json()["is_finished"] == True