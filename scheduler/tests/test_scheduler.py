import time

from RadDeployLib.test_utils.fixtures import *

from .fixtures import *


def test_initiate_flow_in_db(scheduler, dag_flow_context, db, fs):
    f = db.add_db_flow(dag_flow_context)
    assert len(f.db_jobs) == 4
    assert "src" in f.mount_mapping.keys()
    assert fs.exists(f.mount_mapping["src"])


def test_get_and_update_runnable_jobs_by_kwargs_full_dag(scheduler, dag_flow_context, db, fs):
    f = db.add_db_flow(dag_flow_context)
    """ This flow has four jobs
    - docker_kwargs:
      image: busybox
      command: sh -c 'echo $HEST; cd /input; cp -r * /output/1; cp -r * /output/2'
    input_mounts:
      src: /input
    output_mounts:
      CT: /output1
      STRUCT: /output2

  - docker_kwargs:
      image: busybox
      command: sh -c 'echo $HEST; cd /input; cp -r * /output/'
    input_mounts:
      STRUCT: /input
    output_mounts:
      STRUCT_POSTPROCESS1: /output
    gpu: True

  - docker_kwargs:
      image: busybox
      command: sh -c 'echo $HEST; cd /input; cp -r * /output/'
    input_mounts:
      STRUCT: /input
    output_mounts:
      STRUCT_POSTPROCESS: /output

  - docker_kwargs:
      image: busybox
      command: sh -c 'echo $HEST; for fol in /struct /struct1 /ct; do cd $fol; cp -r * /output/; done'
    input_mounts:
      STRUCT_POSTPROCESS: /struct
      STRUCT_POSTPROCESS1: /struct1
      CT: /ct
    output_mounts:
      dst: /output
    """
    jobs = [j for j in scheduler.yield_runnable_jobs()]
    assert len(jobs) == 1 # Only first job should be available

    # Now imitate first job to finish:
    # Now we add input mounts for 2nd and 3rd container.
    # This should make these two available and make the first finished
    db.update_by_kwargs("DBJob", {"id": jobs[0].id}, update_kwargs={"status_code": 0}, first=True)
    db.add_mount_mapping(db_flow_id=f.id,
                         name="STRUCT",
                         uid="ImportanteUID")
    db.add_mount_mapping(db_flow_id=f.id,
                         name="CT",
                         uid="ImportanteCTUID")

    jobs = [j for j in scheduler.yield_runnable_jobs()]
    assert len(jobs) == 2

    # Now imitate 2nd and 3rd job finish:
    # Now we add output mounts for 2nd and 3rd container.
    for job in jobs:
        db.update_by_kwargs("DBJob", {"id": job.id}, update_kwargs={"status_code": 0}, first=True)
    db.add_mount_mapping(db_flow_id=f.id,
                         name="STRUCT_POSTPROCESS",
                         uid="ImportanteUID")
    db.add_mount_mapping(db_flow_id=f.id,
                         name="STRUCT_POSTPROCESS1",
                         uid="ImportanteCTUID")

    # Now only the final container should be available
    jobs = [j for j in scheduler.yield_runnable_jobs()]
    assert len(jobs) == 1

    # Imitate that this runs
    db.update_by_kwargs("DBJob", {"id": jobs[0].id}, update_kwargs={"status_code": 0}, first=True)
    db.add_mount_mapping(db_flow_id=f.id,
                         name="dst",
                         uid="ImportanteCTUID")

    # All jobs should be done now.
    jobs = [j for j in scheduler.yield_runnable_jobs()]
    assert len(jobs) == 0

    flow = db.get_by_kwargs("DBFlow", {"id": f.id}, first=True)
    assert flow.is_finished

def test_fail_job_fails_subsequent(scheduler, dag_flow_context, db, fs):
    f = db.add_db_flow(dag_flow_context)

    jobs = [j for j in scheduler.yield_runnable_jobs()]
    assert len(jobs) == 1 # Only first job should be available

    # Now imitate first job to finish:
    # Now we add input mounts for 2nd and 3rd container.
    # This should make these two available and make the first finished
    db.update_by_kwargs("DBJob",
                                     where_kwargs={"id": jobs[0].id},
                                     update_kwargs={"status_code": 400}, first=True)
    # Check that it is actually status_code == 400
    failed_job = db.get_by_kwargs("DBJob",
                                  where_kwargs={"id": jobs[0].id},
                                  first=True)
    assert failed_job.status_code == 400


    # Will invalidate all subsequent jobs
    scheduler.handle_failed_jobs()

    flow = scheduler.db.get_by_kwargs("DBFlow", {"id": f.id}, first=True)
    assert flow.is_failed

    for j in flow.db_jobs:
        assert j.status_code == 400

def test_yield_finished_flows(scheduler, dag_flow_context, db, fs):
    # Check not finished
    f = db.add_db_flow(dag_flow_context)
    assert not f.is_finished

    # Update all status_codes to finished
    jobs = db.update_by_kwargs("DBJob",
                        where_kwargs={"db_flow_id": f.id},
                        update_kwargs={"status_code": 0})

    for j in jobs:
        assert j.is_finished

    flows = [f for f in scheduler.yield_finished_flows()]
    assert len(flows) == 1

def test_handle_timeout(scheduler, dag_flow_context_1sec_timeout, db, fs):
    # Check not finished

    f = db.add_db_flow(dag_flow_context_1sec_timeout)
    assert not f.is_finished

    time.sleep(1.5)

    scheduler.handle_timeout_jobs() # Should invalidate the flow

    echo_flow = db.get_by_kwargs("DBFlow", {"id": f.id}, first=True)
    assert echo_flow.is_finished
    assert echo_flow.is_failed

    for j in echo_flow.db_jobs:
        assert j.is_timeout
        assert j.status_code == 500
