import os

import pytest

from db import Database

from scheduling import Scheduler
from RadDeployLib.test_utils.fixtures import dag_flow_context

@pytest.fixture
def db(tmpdir):
    database_url = f'sqlite:///{os.path.join(tmpdir, 'scheduler.db')}'
    return Database(database_url=database_url)


@pytest.fixture
def db_flow(db, dag_flow_context):
    db_flow = db.add_db_flow(dag_flow_context)
    return db_flow


@pytest.fixture
def scheduler(db):
    return Scheduler(database=db,
                     log_level=10)
