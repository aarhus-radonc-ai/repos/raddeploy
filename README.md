# RadDeploy
RadDeploy is an extendable micro-service-oriented framework to executed containerized workflows on DICOM files.
Though RadDeploy can be extensively configured, it is designed to provide basic functionality with as little user configuration
as possible - even though this sometimes may come at the price of reduced performance.

## Intended Use
RadDeploy is provided as is and without any responsibility or warrenty by the authors. The author does not considered it a medical device under EU's Medical Device Regulation (MDR),
as it only facilitate recieving of, execution of docker containers on and finally sending of DICOM files. RadDeploy in it self does not manipulate any of the received data. 
Operations on data are limited to storing, lossless compression, mounting to containers, receiving and sending of dicom files. 

If used in a clinical/research setting, beaware that the docker containers should comply to MDR or any local legislation regarding medical devices.

## Quick start and documentations
Head over to the [wiki](https://gitlab.com/aarhus-radonc-ai/repos/raddeploy/-/wikis/home) to find everything you need to get started with RadDeploy.
