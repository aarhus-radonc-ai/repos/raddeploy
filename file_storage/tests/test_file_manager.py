import os
import time
from io import BytesIO

import pytest

from file_manager import FileManager


@pytest.fixture
def temp_bytes_io():
    return BytesIO(b"Hello World")


@pytest.fixture
def file_manager_non_deleting(tmpdir):
    fm = FileManager(base_dir=tmpdir, log_level=10)
    fm.start()
    yield fm
    fm.stop()


def test_file_manager_non_deleting(temp_bytes_io, file_manager_non_deleting):
    fm = file_manager_non_deleting
    print(fm.base_dir)
    print(os.listdir(fm.base_dir))
    # Post
    uid = fm.post_file(temp_bytes_io)
    assert uid
    print(uid)
    print(os.listdir(fm.base_dir))


    # Exist
    time.sleep(1)
    assert fm.file_exists(uid)

    # Clone
    clone_uid = fm.clone_file(uid)
    assert uid != clone_uid
    assert fm.file_exists(clone_uid)

    # Hash
    assert fm.get_hash(uid) == fm.get_hash(clone_uid)

    # Delete
    fm.delete_file(uid)
    assert not fm.file_exists(uid)
    fm.delete_file(clone_uid)
    assert not fm.file_exists(clone_uid)


@pytest.fixture
def file_manager_deleting(tmpdir):
    fm = FileManager(base_dir=tmpdir,
                     delete_files_after=2,
                     log_level=10)
    fm.start()
    time.sleep(1)
    yield fm
    fm.stop()


def test_file_manager_deleting(temp_bytes_io, file_manager_deleting):
    fm = file_manager_deleting
    t0 = time.time()
    uid = fm.post_file(temp_bytes_io)
    counter = 0
    while counter < 20:
        counter += 1
        print((time.time() - t0))
        if (time.time() - t0) > fm.delete_files_after:
            print(uid)
            assert not fm.file_exists(uid)
            break
        time.sleep(5)


if __name__ == "__main__":
    pytest.main()
