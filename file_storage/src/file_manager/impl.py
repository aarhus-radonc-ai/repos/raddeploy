import logging
import os
import time
import uuid
from io import BytesIO

from file_manager.delete_daemon import DeleteDaemon

from RadDeployLib.fs.utils import hash_file


class FileManager:
    def __init__(self,
                 base_dir,
                 suffix: str = ".tar",
                 log_level: int = 20,
                 delete_files_after: int = -1):
        self.running = False
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)
        self.base_dir = base_dir
        os.makedirs(self.base_dir, exist_ok=True)
        self.suffix = suffix
        self.delete_files_after = delete_files_after
        self.delete_daemon = DeleteDaemon(base_dir=base_dir,
                                          delete_files_after=delete_files_after,
                                          log_level=log_level)

    def start(self, blocking: bool = False):
        self.running = True
        self.delete_daemon.start()

        while self.running and blocking:
            time.sleep(1)

    def stop(self):
        self.running = False
        self.logger.info("Stopping file manager")
        self.delete_daemon.stop()
        self.delete_daemon.join()

    def get_hash(self, uid: str):
        self.logger.debug(f"Serving hash of: {uid}")
        return hash_file(str(self.get_file_path(uid)))

    def get_file(self, uid: str):
        self.logger.debug(f"Serving file with uid: {uid}")
        return self.get_file_path(uid)

    def delete_file(self, uid: str):
        self.logger.debug(f"Deleting file with uid: {uid}")
        os.remove(self.get_file_path(uid))
        return "success"

    def clone_file(self, uid: str):
        new_uid = str(uuid.uuid4())
        self.logger.debug(f"Clone file on uid: {uid} to new uid: {new_uid}")
        os.link(self.get_file_path(uid), self.get_file_path(new_uid))
        return new_uid

    def post_file(self, tar_file: BytesIO):
        uid = str(uuid.uuid4())
        self.logger.debug(f"Putting file on uid: {uid}")

        p = self.get_file_path(uid)
        with open(p, "wb") as writer:
            self.logger.debug(f"Writing file with uid: {uid} to path: {p}")
            writer.write(tar_file.read())

        tar_file.close()
        return uid

    def get_file_path(self, uid):
        return os.path.join(self.base_dir, uid + self.suffix)

    def get_uid_from_path(self, path: str):
        return path.replace(self.suffix, "").replace(self.base_dir, "").strip("/")

    def file_exists(self, uid):
        return os.path.isfile(self.get_file_path(uid))
