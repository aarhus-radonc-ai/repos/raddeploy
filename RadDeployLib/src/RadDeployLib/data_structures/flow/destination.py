from typing import List

from pydantic import BaseModel

class Source(BaseModel):
    host: str
    port: int
    ae_title: str

class Destination(Source):
    patterns: List[str] = ["^dst"]

