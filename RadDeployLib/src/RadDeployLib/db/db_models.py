import datetime

import sqlalchemy
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

class Base(DeclarativeBase):
    id: Mapped[int] = mapped_column(primary_key=True, unique=True, autoincrement=True)
    ts: Mapped[datetime.datetime] = mapped_column(sqlalchemy.types.DateTime(timezone=True), default=datetime.datetime.now)
