import logging
import os
import time
from pydoc import locate
from typing import List, Type, Dict

import sqlalchemy
from sqlalchemy import desc
from sqlalchemy.orm import sessionmaker, scoped_session, DeclarativeBase, Mapped, mapped_column


class DatabaseBoilerplate:
    def __init__(self,
                 database_url: str,
                 declarative_base: DeclarativeBase,
                 db_model_module: str,
                 log_level=10):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)
        self.db_model_module=db_model_module
        self.declarative_base = declarative_base
        self.database_url = database_url
        self.engine = sqlalchemy.create_engine(self.database_url, pool_pre_ping=True, future=True)

        # Create schema if the database is empty
        t0 = time.time()
        while (time.time() - t0) < 120:
            try:
                self._initialize_database()
                self.logger.info("Success initializing DB")

                break
            except Exception as e:
                self.logger.error("Error initializing DB")
                time.sleep(5)
        else:
            raise Exception("Timeout initializing DB")


        self.session_maker = sessionmaker(bind=self.engine, expire_on_commit=False)
        self.Session = scoped_session(self.session_maker)

    def _initialize_database(self):
        if "sqlite" in self.database_url:
            os.makedirs(os.path.dirname(self.database_url.split("///", maxsplit=1)[-1]), exist_ok=True)

        insp = sqlalchemy.inspect(self.engine)
        if not insp.has_table(list(self.declarative_base.metadata.tables.keys())[0]):
            self.logger.info("No tables found, schema created.")
            self.declarative_base.metadata.create_all(self.engine)
        else:
            pass

    def _infer_type(self, t: str):
        try:
            return locate(self.db_model_module + "." + t)
        except Exception as e:
            self.logger.error("Type in DB call could not be determined")
            raise e

    def add(self, obj):
        with self.Session() as session:
            session.add(obj)
            session.commit()
            session.refresh(obj)
            return obj

    def get_by_kwargs(self, cls: Type[DeclarativeBase] | str, where_kwargs: Dict = None, first: bool = False, n_recent = None):
        if not where_kwargs:
            where_kwargs = {}
        if isinstance(cls, str):
            cls = self._infer_type(cls)
        with self.Session() as session:
            if first:
                return session.query(cls).filter_by(**where_kwargs).first()
            elif n_recent:
                return session.query(cls).filter_by(**where_kwargs).order_by(desc(cls.id)).limit(n_recent).all()
            else:
                return session.query(cls).filter_by(**where_kwargs).all()

    def get_all(self, cls) -> List:
        with self.Session() as session:
            return session.query(cls).all()

    def update_by_kwargs(self, cls: Type[DeclarativeBase] | str, where_kwargs: Dict, update_kwargs: Dict, first: bool = False):
        if not where_kwargs:
            where_kwargs = {}

        if isinstance(cls, str):
            cls = self._infer_type(cls)

        objs = []
        with self.Session() as session:
            for obj in session.query(cls).filter_by(**where_kwargs):
                for k, v in update_kwargs.items():
                    obj.__setattr__(k, v)

                session.commit()
                session.refresh(obj)
                if first:
                    return obj
                else:
                    objs.append(obj)
        return objs
