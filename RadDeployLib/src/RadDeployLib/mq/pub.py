import functools
import queue
import time
import traceback
from typing import Tuple

from .mq_models import PubModel
from .base import MQBase
from RadDeployLib.mq.mq_models import PublishContext


class MQPub(MQBase):
    def __init__(self,
                 rabbit_hostname: str,
                 rabbit_port: int,
                 in_queue: queue.Queue[Tuple[PubModel, PublishContext]] | None = None,
                 log_level: int = 20):
        super().__init__(rabbit_hostname=rabbit_hostname,
                         rabbit_port=rabbit_port,
                         log_level=log_level)

        self.in_queue = in_queue if in_queue is not None else queue.Queue()

        self._message_number = 0

    def __del__(self):
        self.stop()

    def add_publish_message(self, pub_model: PubModel, pub_context: PublishContext):
        self.in_queue.put((pub_model, pub_context))

    def publish_message_callback(self, pub_model: PubModel, pub_context: PublishContext):
        cb = functools.partial(self.publish_message, pub_model=pub_model, pub_context=pub_context)
        self._connection.add_callback_threadsafe(cb)

    def publish_message(self, pub_model: PubModel, pub_context: PublishContext):
        # Declare Exchanges
        self.setup_exchange(exchange=pub_model.exchange,
                            exchange_type=pub_model.exchange_type)

        self.basic_publish(exchange=pub_model.exchange,
                           routing_key=pub_model.routing_key_values[pub_context.pub_model_routing_key],
                           body=pub_context.body,
                           reply_to=pub_context.reply_to,
                           priority=pub_context.priority)

        self._message_number += 1

    def run(self):
        self.running = True
        while self.running:
            while not self._connection:
                try:
                    self.connect()
                except Exception as e:
                    self.logger.error(str(e))
                    self.logger.error("Reconnecting to RabbitMQ in 5 seconds...")
                    time.sleep(5)

            self.logger.debug("Connected to RabbitMQ")

            self.logger.info('Enabling delivery confirmations')
            self._channel.confirm_delivery()

            self.logger.info('Starting publishing')
            while self.running:
                time_zero = time.time()
                while time.time() - time_zero < (self.heartbeat / 4):
                    try:
                        self.process_event_data()
                        elem: Tuple[PubModel, PublishContext] = self.in_queue.get(timeout=2)
                        self.publish_message(*elem)
                    except queue.Empty:
                        if self.running and self._connection:
                            self.process_event_data()
                    except KeyboardInterrupt:
                        if self.running:
                            self.stop()
                    except Exception as e:
                        self.logger.error(traceback.format_exc())
                        raise e


