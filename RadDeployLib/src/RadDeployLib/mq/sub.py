# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205
import logging
import threading
import time
import traceback
from typing import List, Dict, Iterable

from .base import MQBase
from RadDeployLib.mq.mq_models import PublishContext
from .mq_models import PubModel, SubModel


class MQSub(MQBase):
    """
    A consumer class that connects to RabbitMQ, subscribes to message queues, and processes incoming messages
    using a specified worker function. The class supports subscribing to multiple exchanges and routing keys,
    and it also handles message publishing on different exchanges based on the results from the worker function.

    The class manages RabbitMQ connections, queue declarations, message consumption, and acknowledgment. It also
    allows publishing results to multiple exchanges with specific routing keys.

    Inherits from:
        MQBase: Provides core functionality for connecting and interacting with RabbitMQ.

    Attributes:
        rabbit_hostname (str): The hostname of the RabbitMQ server.
        rabbit_port (int): The port number of the RabbitMQ server.
        sub_models (List[SubModel]): A list of subscription models defining exchanges and routing keys for consuming messages.
        pub_models (List[PubModel]): A list of publish models defining exchanges and routing keys for publishing results.
        work_function (callable): A function that processes incoming messages. It should return an iterable of PublishContext objects.
        sub_prefetch_value (int): The number of messages to prefetch for each subscriber.
        sub_queue_kwargs (Dict, optional): Additional arguments for queue configuration.
        log_level (int): The logging level for this class. Default is INFO (level 20).
    """

    def __init__(self,
                 rabbit_hostname: str,
                 rabbit_port: int,
                 sub_models: List[SubModel],
                 pub_models: List[PubModel],
                 work_function: callable,
                 sub_prefetch_value: int,
                 sub_queue_kwargs: Dict | None = None,
                 log_level: int = 20):

        super().__init__(rabbit_hostname=rabbit_hostname,
                         rabbit_port=rabbit_port,
                         log_level=log_level)


        # Which function to run on msg
        self.work_function = work_function

        # Prefetch value - usually set to 1
        self.sub_prefetch_value = sub_prefetch_value

        # Threads of jobs. Should usually only contain one
        self._thread_lock = threading.Lock()
        self._threads = []

        # SubModels define which exchanges and routing_keys to listen to
        self.sub_models = {sm.exchange: sm for sm in sub_models}

        # PubModels define which exchanges and routing_keys to publish to
        self.pub_models = {pm.exchange: pm for pm in pub_models}

        # Specs on the queue which receives from the SubModels
        if sub_queue_kwargs:
            self.sub_queue_kwargs = sub_queue_kwargs
        else:
            self.sub_queue_kwargs = {}

        self.queue = None

    def declare_pub_and_sub_models(self):
        """
        Declare exchanges for both subscription and publication models, and bind queues to the
        appropriate routing keys for each subscription model.
        """
        for sub in self.sub_models.values():
            self.logger.debug(f"Setting up subscription model: {sub}")
            self.logger.debug(f"Declaring exchange: {sub.exchange} of type {sub.exchange_type}")

            # Set up exchange for subscription model
            self.setup_exchange(exchange=sub.exchange, exchange_type=sub.exchange_type)

            # Bind the queue to each routing key associated with this subscription model
            for rk in sub.routing_keys:
                # Declare queue
                self.bind_queue(queue=self.queue, exchange=sub.exchange, routing_key=rk)

        # Declare exchanges for each publication model
        for pub in self.pub_models.values():
            self.logger.debug(f"Setting up publication model: {pub}")
            self.logger.debug(f"Declaring exchange: {pub.exchange} of type {pub.exchange_type}")

            # Set up exchange for publication model
            self.setup_exchange(exchange=pub.exchange, exchange_type=pub.exchange_type)


    def run(self):
        """
        Main loop that manages RabbitMQ connection, declares exchanges and queues, and processes incoming messages.
        """
        self.running = True

        while self.running:
            self.logger.debug("Establishing RabbitMQ connection.")
            self.connect() # Handles timeout and reconnect

            # Setup queue with specified configuration
            self.queue = self.setup_queue(**self.sub_queue_kwargs)
            self.logger.debug(f"Queue setup complete: {self.queue}")

            # Declare exchanges and bind queues
            self.declare_pub_and_sub_models()

            # Set prefetch count to control message flow
            self.logger.debug(f"Setting prefetch value: {self.sub_prefetch_value}")
            self._channel.basic_qos(prefetch_count=self.sub_prefetch_value)

            # Start consuming messages from the queue
            self.logger.debug(f"Starting to consume messages from queue: {self.queue}")
            self._channel.basic_consume(queue=self.queue, on_message_callback=self.on_message)

            self.logger.info(' [*] Waiting for messages')

            self.logger.info('Enabling delivery confirmations')
            self._channel.confirm_delivery()

            while self.running:
                self.process_event_data()
                time.sleep(0.1)

            if self._connection and not self.running:
                self.close_connection()
                return


    def publish_on_all_pub_models(self,
                                  result: PublishContext):
        """
        Publish the result on all configured publication models using the corresponding routing keys.

        :param PublishContext result: The result object containing the message to publish.
        """
        self.logger.debug(f"Publishing result to all publication models with context: {result}")
        for pub_model in self.pub_models.values():
            routing_key = pub_model.routing_key_values[result.pub_model_routing_key]
            self.logger.debug(f"Publishing on exchange: {pub_model.exchange} with routing key: {routing_key}")

            # Publish message to the exchange with appropriate routing key
            self.basic_publish_callback(exchange=pub_model.exchange,
                                        routing_key=pub_model.routing_key_values[result.pub_model_routing_key],
                                        body=result.body,
                                        priority=result.priority)

        self.logger.info("Finished publishing to all publication models.")

    def on_message(self, _unused_channel, basic_deliver, properties, body):
        """
        Callback function invoked when a message is received. Processes the message in a separate thread.

        :param _unused_channel: The unused channel.
        :param basic_deliver: Basic delivery information.
        :param properties: Message properties.
        :param body: The body of the received message.
        """
        self.logger.debug(f"Received message #{basic_deliver.delivery_tag} from {properties.app_id}.")

        # Start a new thread to handle the work function for the received message
        t = threading.Thread(target=self.work_function_wrapper,
                             args=(basic_deliver, body))
        t.start()

    def work_function_wrapper(self, basic_deliver, body):
        """
        Wrapper around the work function to handle message processing and error handling.

        :param basic_deliver: Basic delivery information.
        :param body: The body of the received message.
        """
        try:
            self.logger.debug(f"Processing message #{basic_deliver.delivery_tag}")

            # Call the user-provided work function to process the message
            results: Iterable[PublishContext] = self.work_function(basic_deliver, body)

            # Publish results for each success case
            for result in results:
                self.publish_on_all_pub_models(result=result)  # Routing key on success

        except Exception as e:
            # Log the exception and publish an error context if an exception occurs
            self.logger.error(f"Error while processing message #{basic_deliver.delivery_tag}: {str(e)}")
            self.logger.error(str(traceback.format_exc()))
            self.publish_on_all_pub_models(
                result=PublishContext(body=body, pub_model_routing_key="ERROR"))  # routing key on error
        finally:
            # Acknowledge the message after processing
            self.logger.debug('Acknowledging message {}'.format(basic_deliver.delivery_tag))
            self.acknowledge_message_callback(basic_deliver.delivery_tag)
