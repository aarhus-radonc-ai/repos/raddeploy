import os
from io import BytesIO

import pandas as pd
import pytest

from RadDeployLib.data_structures.flow import Destination, Flow, Model
from RadDeployLib.data_structures.service_contexts import SCPContext, FlowContext, JobContext
from RadDeployLib.mq import MQBase
from RadDeployLib.test_utils.mock_classes import MockFileStorageClient


@pytest.fixture
def fs():
    return MockFileStorageClient()


@pytest.fixture
def mq_base():
    return MQBase(rabbit_hostname="rabbitmq", rabbit_port=5672, log_level=10).connect()


@pytest.fixture
def flow_dir():
    from RadDeployLib.test_utils.test_data import flows
    return os.path.dirname(flows.__file__)


@pytest.fixture
def scan_dir():
    from RadDeployLib.test_utils.test_data import scans
    return os.path.dirname(scans.__file__)


@pytest.fixture
def scp_tar_path():
    from RadDeployLib.test_utils.test_data import scp_tar
    return os.path.join(os.path.dirname(scp_tar.__file__), "scp.tar")


@pytest.fixture
def scp_tar(scp_tar_path):
    with open(scp_tar_path, "rb") as scp_tar_file:
        file = BytesIO(scp_tar_file.read())
    file.seek(0)
    return file


@pytest.fixture
def destination():
    return Destination(host="localhost", port=1234, ae_title="test")


@pytest.fixture
def dag_flow_path(flow_dir):
    return os.path.join(flow_dir, 'dag_flow.yaml')


@pytest.fixture
def dag_flow(dag_flow_path):
    return Flow.from_file(dag_flow_path)


@pytest.fixture
def scp_context(fs, scp_tar, destination):
    uid = fs.post(scp_tar)
    return SCPContext(src_uid=uid, sender=destination)


@pytest.fixture
def dag_flow_context(scp_context, dag_flow):
    return FlowContext(**scp_context.model_dump(),
                       flow=dag_flow,
                       context={})

@pytest.fixture
def dag_flow_context_1sec_timeout(scp_context, dag_flow):
    for i, _ in enumerate(dag_flow.models):
        dag_flow.models[i].timeout = 1

    return FlowContext(**scp_context.model_dump(),
                       flow=dag_flow,
                       context={})




@pytest.fixture
def model(scp_context, dag_flow):
    return Model(
        docker_kwargs={"image": "hello-world"},
    )


@pytest.fixture
def job_context(model, fs, scp_tar):
    return JobContext(model=model,
                      id=123,
                      mount_mapping={"src": fs.post(scp_tar)},
                    )
