#!/bin/sh
docker compose -f docker-compose_testing.yaml down --remove-orphans
docker compose -f docker-compose_testing.yaml build
docker compose -f docker-compose_testing.yaml up -d rabbit
if [ -z "$1" ]
  then
    for service in file_storage scheduler fingerprinter consumer storescu storescp
      do
        docker compose -f docker-compose_testing.yaml run $service 'pytest -s ../tests' || exit 1
      done
  exit 0
  fi

for service in "$@"
  do
    docker compose -f docker-compose_testing.yaml run $service 'pytest -s ../tests' || exit 1
  done

echo "All tests passed"