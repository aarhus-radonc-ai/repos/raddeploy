import time

import yaml
from main import Main

from RadDeployLib.test_utils.fixtures import *
import os
import pytest

@pytest.fixture
def config(tmpdir, flow_dir):
    with open(os.path.join(os.path.dirname(__file__), "..", "conf.d", "00_default_config.yaml"), "r") as r:
        config = yaml.safe_load(r)

    config["LOG_DIR"] = tmpdir
    config["LOG_LEVEL"] = 10
    config["FLOW_DIRECTORY"] = flow_dir
    config["FILE_STORAGE_URL"] = ""

    return config

@pytest.fixture
def main(config):
    main = Main(config, MockFileStorageClient)
    main.start()
    yield main
    main.stop()

def test_main(mq_base, scp_tar, config, main):
    # Post a tar file to the filestorage to mimic something coming from the SCP
    src_uid = main.fs.post(scp_tar)
    scp_tar.seek(0)

    # Exchange should already have been setup, but best practice is to always to it.
    mq_base.setup_exchange("storescp", "topic")
    mq_base.setup_exchange("fingerprinter", "topic")

    # mq_base must also make a queue and catch what is published from mq_sub
    q_out = mq_base.setup_queue_and_bind("fingerprinter", routing_key="success")

    # Generate the scp_context, which the fingerprinter should retrieve and process.
    scp_context = SCPContext(src_uid=src_uid, sender=Destination(host="localhost", port=1234, ae_title="test"))

    # Publish to the exchange and routing key that the mq_sub is listening to.
    q_in = mq_base.setup_queue_and_bind("storescp", routing_key="success")
    mq_base.basic_publish(exchange="storescp",
                          routing_key="success",
                          body=scp_context.model_dump_json())

    # Check that a seperate queue can in fact receive the data
    t0 = time.time()
    while (time.time() - t0) < 10:
        try:
            method_frame, header_frame, body = mq_base._channel.basic_get(q_in, auto_ack=True)
            assert method_frame
            break
        except:
            time.sleep(0.1)
    else:
        raise Exception("Timed out")

    t0 = time.time()
    while (time.time() - t0) < 10:
        try:
            method_frame, header_frame, body = mq_base._channel.basic_get(q_out, auto_ack=True)
            assert method_frame
            break
        except:
            time.sleep(0.1)
    else:
        raise Exception("Timed out")