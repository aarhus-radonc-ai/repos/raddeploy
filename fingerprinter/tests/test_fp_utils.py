from RadDeployLib.fp import (parse_fingerprints, generate_df_from_tar, generate_flow_specific_tar,
                             slice_dataframe_to_triggers, parse_links_from_file, parse_fingerprint_link)
from RadDeployLib.test_utils.fixtures import *
import tempfile
test_dir = os.path.dirname(__file__)


def test_parse_fingerprints(flow_dir):
    flows = parse_fingerprints(flow_dir)
    for flow in flows:
        isinstance(flow, Flow)

@pytest.fixture
def flow_url():
    return "https://gitlab.com/aarhus-radonc-ai/repos/raddeploy/-/raw/ed64202553dff62db254bb3c54bc35e775ca4b34/examples/RadDeploy/conf/fingerprinter/flows/total_segmentator.yaml"

@pytest.fixture
def flow_file_str(flow_url):
    return f"""
# This is a testedy test!
{flow_url}
# Another interesting comment
"""

def test_parse_fingerprint_link(flow_url):
    flow = parse_fingerprint_link(flow_url)
    assert isinstance(flow, Flow)

def test_parse_fingerprint_file(flow_file_str):
    i, path = tempfile.mkstemp(suffix=".link")
    with open(path, mode="w") as g:
        g.write(flow_file_str)

    links = parse_links_from_file(path)
    for link in links:
        test_parse_fingerprint_link(link)

def test_parse_fingerprint_file_twice(flow_file_str):
    i, path = tempfile.mkstemp(suffix=".link")
    with open(path, mode="w") as g:
        g.write(flow_file_str)
        g.write(flow_file_str)

    links = parse_links_from_file(path)
    for link in links:
        test_parse_fingerprint_link(link)
def test_generate_df_from_tar(scp_tar):
    df = generate_df_from_tar(scp_tar)
    assert "path" in df.columns
    assert 'ct.dcm' in df["path"].tolist()
    assert 'mr.dcm' in df["path"].tolist()


def test_generate_flow_specific_tar(scp_tar):
    df = generate_df_from_tar(scp_tar)
    sliced_tar = generate_flow_specific_tar(tar_file=scp_tar, sliced_df=df[df["Modality"] == "MR"])
    sliced_df = generate_df_from_tar(sliced_tar)
    assert sliced_df["path"].tolist() == [
        "MR_1.3.46.670589.11.71891.5.0.8000.2022062210324747248_1.3.46.670589.11.71891.5.0.8000.2022062210385801250.dcm"]

    sliced_tar = generate_flow_specific_tar(tar_file=scp_tar, sliced_df=df[df["Modality"].isin(["CT", "MR"])])
    sliced_df = generate_df_from_tar(sliced_tar)
    assert sliced_df["path"].tolist() == [
        'CT_1.3.6.1.4.1.40744.29.33371661027192187491509798061184654147_1.3.6.1.4.1.40744.29.179461095576983824275091091253440398486.dcm',
        "MR_1.3.46.670589.11.71891.5.0.8000.2022062210324747248_1.3.46.670589.11.71891.5.0.8000.2022062210385801250.dcm"]


def test_slice_dataframe_to_triggers(scp_tar):
    ct_trigger = [
        {"Modality": ["CT"]}
    ]
    df = generate_df_from_tar(scp_tar)
    sliced_df = slice_dataframe_to_triggers(triggers=ct_trigger, ds=df)

    sliced_tar = generate_flow_specific_tar(tar_file=scp_tar, sliced_df=sliced_df)
    sliced_tar_df = generate_df_from_tar(sliced_tar)
    assert set(sliced_tar_df["SeriesInstanceUID"].tolist()) == set(sliced_df["SeriesInstanceUID"].tolist())


if __name__ == '__main__':
    pytest.main()
