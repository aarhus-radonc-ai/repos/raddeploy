import json
import logging
from io import BytesIO
from typing import Iterable, Dict

import pandas as pd
from RadDeployLib.data_structures.service_contexts import FlowContext, SCPContext
from RadDeployLib.fs.client.interface import FileStorageClientInterface
from RadDeployLib.mq import PublishContext
from RadDeployLib.fp import parse_fingerprints, slice_dataframe_to_triggers, generate_flow_specific_tar, \
    generate_df_from_tar


class Fingerprinter:
    def __init__(self,
                 file_storage: FileStorageClientInterface,
                 flow_directory: str,
                 log_level: int = 20):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)

        self.flow_directory = flow_directory
        self.fs = file_storage

    def mq_entrypoint(self, basic_deliver, body) -> Iterable[PublishContext]:
        self.logger.info(self.logger.name)

        scp_context = SCPContext(**json.loads(body.decode()))
        tar_file = self.fs.get(scp_context.src_uid)
        try:
            yield from self.run_fingerprinting(scp_context=scp_context, tar_file=tar_file)
        except Exception as e:
            self.logger.error(str(e))
            raise e
        finally:
            tar_file.close()

    def generate_context(self, df: pd.DataFrame) -> Dict:
        """
        This method extract name and ID of patient
        """
        patient_id = df["PatientID"].to_list()[0]
        patient_name = df["PatientName"].to_list()[0]
        return {
            "patient_id": patient_id,
            "patient_name": patient_name,
        }
    def log(self, level, msg):
        if self.scp_context:
            self.logger.log(level=level, msg=f"Base UID: {self.scp_context.uid} - {msg}")
        else:
            self.logger.log(level=level, msg=msg)

    def run_fingerprinting(self, scp_context: SCPContext, tar_file: BytesIO):
        # Used for formatting of logging.
        self.scp_context = scp_context

        # Generate a dataframe of files in the received tar
        self.log(level=logging.INFO, msg=f"Generating a dataframe files in tar: {scp_context.src_uid}")
        dataframe = generate_df_from_tar(tar_file=tar_file)

        for flow in parse_fingerprints(self.flow_directory):
            # Slices dataframe according to the triggers for a given flow.
            sliced_dataframe = slice_dataframe_to_triggers(dataframe, flow.triggers)

            # If there are still files in the dataframe, there is a match
            if sliced_dataframe is not None:
                self.log(level=logging.INFO, msg=f"Checking flow with name: {flow.name}. MATCH!")

                # Generate a new tar file with only files found in the sliced_dataframe
                flow_tar_file = generate_flow_specific_tar(sliced_df=sliced_dataframe,
                                                           tar_file=tar_file,
                                                           tar_subdir=flow.tar_subdir)

                # Place the new flow-specific tar at the file server
                self.log(level=logging.DEBUG, msg="Posting flow tar on file server")
                file_uid = self.fs.post(flow_tar_file)

                # When posted, the file should be closed.
                flow_tar_file.close()

                # Construct the FlowContext, which can be published
                self.log(level=logging.DEBUG, msg=f"Constructing FlowContext for publication")
                flow_context = FlowContext(flow=flow.model_copy(deep=True),
                                           src_uid=file_uid, # Notice that this is updated to the new file UID.
                                           context=self.generate_context(df=sliced_dataframe), # Contains patient name and ID
                                           sender=scp_context.sender)

                # Publish the new FlowContext
                yield PublishContext(pub_model_routing_key="SUCCESS",
                                              body=flow_context.model_dump_json().encode(),
                                              priority=flow.priority)

            else:
                # If sliced_dataframe contains nothing, there is no match.
                # Publish the SCPContext under routing key "FAIL".
                self.log(level=logging.DEBUG, msg=f"Checking flow with name: {flow.name}. No match")
                yield PublishContext(pub_model_routing_key="FAIL",
                                     body=scp_context.model_dump_json().encode(),
                                     priority=flow.priority)