import queue
import uuid

import pytest
from scp import SCP

from RadDeployLib.data_structures.flow import Source
from scp.models import SCPAssociation


@pytest.fixture
def scp_association():
    return SCPAssociation(
        assoc_id=1,
        sender=Source(host="127.0.0.1",
                           port=10000,
                           ae_title="ae_title"),
        src_uid=str(uuid.uuid4())
    )

@pytest.fixture
def scp_out_queue():
    return queue.Queue()


@pytest.fixture
def scp(scp_out_queue):
    s = SCP(
        out_queue=scp_out_queue,
        port=10000,
        hostname="localhost",
        blacklist_networks=None,
        whitelist_networks=None,
        ae_title="RADDEPLOY",
        pynetdicom_log_level=20,
        log_level=10).start(blocking=False)
    yield s

    s.stop()