from RadDeployLib.data_structures.flow import Source


def generate_source_from_event(event):
    return Source(host=event.assoc.requestor.address,
                       port=event.assoc.requestor.port,
                       ae_title=event.assoc.requestor._ae_title)
