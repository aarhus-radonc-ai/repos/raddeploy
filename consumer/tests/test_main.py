import json
import time

import yaml
from main import Main

from RadDeployLib.test_utils.fixtures import *


@pytest.fixture
def config(tmpdir):
    with open(os.path.join(os.path.dirname(__file__), "..", "conf.d", "00_default_config.yaml"), "r") as r:
        config = yaml.safe_load(r)

    config["LOG_DIR"] = tmpdir
    config["LOG_LEVEL"] = 10
    config["LOG_DIR"] = tmpdir
    config["FILE_STORAGE_URL"] = ""

    return config

@pytest.fixture
def main(config, fs):
    ss = MockFileStorageClient()
    m = Main(
        config=config,
        file_storage=fs,
    )
    m.start(blocking=False)
    # Wait for setting up consuming
    time.sleep(5)
    yield m

    m.stop()


def test_main(mq_base, scp_tar, main, job_context, fs):

    # Exchange should already have been setup, but best practice is to always to it.
    mq_base.setup_exchange("scheduler", "topic")
    mq_base.setup_exchange("consumer", "topic")

    # mq_base must also make a queue and catch what is published from mq_sub
    q_out = mq_base.setup_queue_and_bind("consumer", routing_key="success")

    job_context.model = Model(
        docker_kwargs={"image": "hello-world"}
    )


    # Publish to the exchange and routing key that the mq_sub is listening to.
    mq_base.basic_publish(exchange="scheduler",
                          routing_key="cpu",
                          body=job_context.model_dump_json())
    #
    # Wait for the entry to be processed
    time.sleep(5)

    method_frame, header_frame, body = mq_base._channel.basic_get(q_out, auto_ack=True)
    assert method_frame
