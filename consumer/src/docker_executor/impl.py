import datetime
import json
import logging
import os
import shutil
import tarfile
import tempfile
import threading
import time
import traceback
from io import BytesIO
from typing import Iterable, Dict

import docker
import requests
import yaml
from docker import types, errors

from RadDeployLib.data_structures.flow import Model
from RadDeployLib.data_structures.service_contexts.models import JobContext
from RadDeployLib.fs.client.interface import FileStorageClientInterface
from RadDeployLib.mq import PublishContext

from scheduler_api_client import SchedulerAPIClient



class DockerExecutor(threading.Thread):
    def __init__(self,
                 file_storage: FileStorageClientInterface,
                 worker_device_id: str,
                 log_dir: str,
                 worker_type: str = "CPU",
                 log_level: int = 20,
                 docker_config: Dict | None = None,
                 scheduler_api_client: SchedulerAPIClient | None = None):
        super().__init__()
        self.pub_declared = False
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)

        self.log_dir = log_dir
        os.makedirs(self.log_dir, exist_ok=True)

        self.fs = file_storage

        self.worker_type = worker_type
        self.worker_device_id = worker_device_id
        self.scheduler_api_client = scheduler_api_client

        self.cli = docker.from_env()
        if docker_config:
            self.logger.info(f"Attempting to sign in to {docker_config['registry']}")
            res = self.cli.login(**docker_config)
            self.logger.info(str(res))
            assert res["Status"] == "Login Succeeded"

    def __del__(self):
        self.cli.close()

    def mq_entrypoint(self, basic_deliver, body) -> Iterable[PublishContext]:
        job_context = JobContext(**json.loads(body.decode()))
        if self.scheduler_api_client:
            self.logger.debug("Checking revoke status")
            if self.scheduler_api_client.get_db_job(job_context.id) != -2: # Returns True if job is finished/revoked
                self.logger.info("This job should not be run, SchedulerAPIClient says!")
                return []
            else:
                self.logger.debug("This job may run, SchedulerAPIClient says!")

        self.logger.info(f"Spinning up flow")
        for jc in self.exec_model(job_context):
            yield self._wrap_job_context(jc)
        self.logger.info(f"Finished flow")


    def exec_model(self,
                   job_context: JobContext
                   ) -> JobContext:

        container, kwargs = None, None
        try:
            ###### Tell the world that this job has started ######
            job_context.status_code = -1
            yield job_context

            ###### Unwrap model and input mappings ######
            model: Model = job_context.model

            ###### Should we pull image before exec? ######
            if model.pull_before_exec:
                try:
                    self.logger.info(f"Pulling {model.docker_kwargs['image']}")
                    self.cli.images.pull(*model.docker_kwargs["image"].split(":"))
                except Exception as e:
                    self.logger.error("Could not pull container - is it public?")
                    job_context.status_code = 401
                    yield job_context
                    raise e

            self.logger.info(f"RUNNING CONTAINER TAG: {model.docker_kwargs["image"]}")

            ###### Unwrap docker_kwargs ######
            kwargs = model.docker_kwargs

            ###### generate dummy binds for all mounts ######
            kwargs["volumes"] = []
            if model.config_path:
                kwargs["volumes"].append(
                    f"{tempfile.mkdtemp()}:{os.path.dirname(model.config_path)}"  # Folder for config.
                )  # Default is /config/config.yaml

            if model.flow_path:
                kwargs["volumes"].append(
                    f"{tempfile.mkdtemp()}:{os.path.dirname(model.flow_path)}"  # Folder for flow definition.
                )  # Default is None

            for dst in {*model.input_mounts.values(), *model.output_mounts.values()}:
                kwargs["volumes"].append(f'{tempfile.mkdtemp()}:{dst}')

            ###### Allow GPU usage ######
            if self.worker_type == "GPU":
                kwargs["ipc_mode"] = "host"
                self.logger.debug(f"Uses GPU device {self.worker_device_id}")
                if "DEBUG_DISABLE_GPU" not in model.config.keys():
                    kwargs["device_requests"] = [
                        types.DeviceRequest(device_ids=[self.worker_device_id], capabilities=[['gpu']])]

            # So logs can be pulled
            kwargs["detach"] = True

            # Add docker tag to environment to allow traceability
            if "environment" not in kwargs.keys():
                kwargs["environment"] = {}
            elif isinstance(kwargs["environment"], list):
                kwargs["environment"] = {elem.split("=", maxsplit=1)[0]: elem.split("=", maxsplit=1)[-1] for elem in kwargs["environment"]}

            # Assert that environment is dict by now
            assert isinstance(kwargs["environment"], dict)

            kwargs["environment"]["JOB_ID"] = job_context.id
            kwargs["environment"]["CONTAINER_TAG"] = model.docker_kwargs["image"]
            kwargs["environment"]["FLOW_PATH"] = model.flow_path
            kwargs["environment"]["CONFIG_PATH"] = model.config_path

            # To do: Insert hash of flow definition

            # Create container
            container = self.cli.containers.create(**kwargs)

            # Dumps flow.yaml in folder
            if model.flow_path:
                self.logger.info(f"Dumping flow definition in {model.flow_path}")
                with self.config_to_tar(job_context.flow, model.flow_path) as flow_file:
                    container.put_archive("/", flow_file.read())  # Dump in root. Should end up the right place

            # Dumps config.yaml in folder
            if model.config_path:
                self.logger.info(f"Dumping config in {model.config_path}")
                with self.config_to_tar(model.config, model.config_path) as config_file:
                    container.put_archive("/", config_file.read())  # Dump in root. Should end up the right place

            # Mount inputs
            # Remap to use actual uid from the file_storage
            for name, dst in model.input_mounts.items():
                input_file = self.fs.get(job_context.mount_mapping[name])
                container.put_archive(dst, input_file) # dst is the path and latter arg is gets uid

            # Run the container
            container.start_time = time.time()
            container.start()

            # Grab the output and log to job specific logger
            logging_thread = threading.Thread(target=self.catch_logs_from_container, args=(job_context, container))
            logging_thread.start()

            # Wait for job to execute - with timeout. If not finished, it will be killed.
            while (time.time() - container.start_time) < model.timeout:
                try:
                    result = container.wait(timeout=10)  # Blocks for 10 seconds...
                    job_context.status_code = result["StatusCode"]

                    # Save output dirs
                    for src, dst in model.output_mounts.items():
                        tar = self.get_archive(container=container, path=dst)
                        uid = self.fs.post(tar)
                        job_context.mount_mapping[src] = uid
                    break
                except requests.exceptions.ConnectionError:
                    self.logger.debug("Checking timeout status before checking container status again")
                    ###### Tell the world that we are still running ######
                    yield job_context
                except Exception as e:
                    self.logger.error(str(e))
                    raise e
            else:
                job_context.status_code = 500  # timeout

        except Exception as e:
            self.logger.error(traceback.format_exc())
            raise e
        finally:
            # Clean up threads.
            if container:
                threading.Thread(target=self.delete_container, args=(container.short_id,)).start()
            if kwargs:
                threading.Thread(target=self.remove_temp_dirs, args=(kwargs["volumes"],)).start()

            # Yield finished updated job_context
            yield job_context

    def catch_logs_from_container(self, job_context: JobContext, container):
        logger_name = (f"{datetime.datetime.now().isoformat()}_{job_context.uid}_"
                           f"{job_context.id}_{job_context.model.docker_kwargs["image"].replace("/", "-")}")

        # Create logger which only writes to directory
        with open(os.path.join(self.log_dir, logger_name + ".log"), "a") as f:
            for line in container.logs(stream=True):
                l = line.decode()
                f.write(l)
                f.flush()

    def delete_container(self, container_id):
        counter = 0
        while counter < 5:
            try:
                self.logger.debug(f"Attempting to remove container {container_id}")
                c = self.cli.containers.get(container_id)
                c.remove(force=True)
                return
            except errors.NotFound:
                return
            except Exception as e:
                self.logger.debug(f"Failed to remove {container_id}. Trying again in 5 sec...")
                counter += 1
                time.sleep(5)

    @staticmethod
    def remove_temp_dirs(volumes):
        # Delete dummy directories from mounts. These are empty, so no loss if this fails for some reason.
        for src_dst in volumes:
            src, dst = src_dst.split(":")
            if os.path.exists(src):
                shutil.rmtree(src)

    def get_archive(self, container, path):
        output, stats = container.get_archive(path=path)

        output_tar = BytesIO()  # Write to a format I understand
        for chunk in output:
            output_tar.write(chunk)
        output_tar.seek(0)

        return self.postprocess_output_tar(path, tarf=output_tar)

    @staticmethod
    def config_to_tar(config_dict: Dict, config_path: str):
        info = tarfile.TarInfo(name=config_path)
        dump = BytesIO(yaml.dump(config_dict).encode())
        dump.seek(0, 2)
        info.size = dump.tell()
        dump.seek(0)

        file = BytesIO()
        with tarfile.TarFile.open(fileobj=file, mode="w") as tf:
            tf.addfile(info, dump)
            dump.close()

        file.seek(0)
        return file

    @staticmethod
    def postprocess_output_tar(output_dir: str, tarf: BytesIO):
        """
        Removes one layer from the output_tar - i.e. the /output/
        """
        """
        Removes one layer from the output_tar - i.e. the /output/
        If dump_logs==True, container logs are dumped to container.log
        """

        # Unwrap container tar to temp dir
        with tempfile.TemporaryDirectory() as tmpd:
            with tarfile.TarFile.open(fileobj=tarf, mode="r|*") as container_tar_obj:
                container_tar_obj.extractall(tmpd, filter="data")

            # Make a new temp tar.gz
            new_tar_file = BytesIO()
            with tarfile.TarFile.open(fileobj=new_tar_file, mode="w") as new_tar_obj:
                # Walk directory from output to strip it away the base dir
                to_del = "/" + tmpd.strip("/") + "/" + output_dir.strip("/") + "/"
                for fol, subs, files in os.walk(os.path.join(tmpd)):
                    for file in files:
                        path = os.path.join(fol, file)
                        new_path = path.replace(to_del, "")
                        new_tar_obj.add(path, arcname=new_path)

            new_tar_file.seek(0)  # Reset pointer
            return new_tar_file  # Ready to ship directly to DB

    def _wrap_job_context(self, job_context):
        if job_context.status_code <= 0:
            return PublishContext(body=job_context.model_dump_json().encode(), pub_model_routing_key="SUCCESS")
        else:
            return PublishContext(body=job_context.model_dump_json().encode(), pub_model_routing_key="FAIL")
