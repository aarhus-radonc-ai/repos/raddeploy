import requests


class SchedulerAPIClient:
    def __init__(self, base_url):
        self.base_url = base_url
        self.revoke_url = base_url + "{}"

    def get_db_job(self, id: int):
        res = requests.get(self.base_url + "/db_job/{}".format(id))
        res.raise_for_status()
        return res.json()