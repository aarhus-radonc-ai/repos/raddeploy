FROM python:3.12-slim AS raddeploy_base
LABEL authors="Mathis Ersted Rasmussen"

RUN apt update && apt install tzdata -y
ENV TZ="Europe/Copenhagen"

# Setup env paths
ENV APP_BASE_DIR="/opt/RadDeploy"
ENV DEPENDENCIES_DIR=$APP_BASE_DIR/dependencies

# Make directories
RUN mkdir -p $APP_BASE_DIR $DEPENDENCIES_DIR
RUN chmod 753 -R $APP_BASE_DIR

# Install RadDeployLib
ADD ./RadDeployLib $DEPENDENCIES_DIR/RadDeployLib
RUN pip install -e $DEPENDENCIES_DIR/RadDeployLib

FROM raddeploy_base
ARG SERVICE_NAME

# Environment variables
ENV APP_DIR=/opt/RadDeploy/${SERVICE_NAME}
ENV CONF_DIR=$APP_DIR/conf.d
ENV CURRENT_CONF=$APP_DIR/current_config.yaml

# Install requirements
ADD ${SERVICE_NAME}/requirements /requirements
RUN pip install -r /requirements

# Copy code
COPY $SERVICE_NAME $APP_DIR

# Go to exec space
WORKDIR $APP_DIR/src

# Vars
ENV PYTHONUNBUFFERED=1

ENTRYPOINT ["/usr/bin/bash", "-c"]
CMD ["python3 main.py"]